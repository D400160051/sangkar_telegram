#include <ESP8266WiFi.h>
#include <UniversalTelegramBot.h>
#include <WiFiClientSecure.h>
#include <NTPClient.h>
#include <EEPROM.h>

const char* ssid = "namaSSID";
const char* password = "passwordSSID";
const char* telegramToken = "YOUR_TELEGRAM_BOT_TOKEN";
const long utcOffsetInSeconds = 25200;  // Offset waktu Indonesia Barat (GMT+7)

WiFiClientSecure client;
UniversalTelegramBot bot(telegramToken, client);
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "id.pool.ntp.org", utcOffsetInSeconds);

const int buzzerPin = D4;  // Pin B2
const int eepromAddress = 0;  // Alamat EEPROM untuk menyimpan alarm

unsigned long lastCheckTime = 0;
unsigned long checkInterval = 300000; // Interval pengecekan Telegram (5 menit)

void setup() {
  Serial.begin(115200);
  pinMode(buzzerPin, OUTPUT);
  digitalWrite(buzzerPin, LOW);

  WiFi.begin(ssid, password);
  timeClient.begin();

  // Mulai koneksi Wi-Fi
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  Serial.println("Connected to WiFi");

  // Memulai monitoring waktu
  timeClient.update();
  loadAlarmFromEEPROM();
}

void loop() {
  unsigned long currentTime = millis();
  if (currentTime - lastCheckTime >= checkInterval) {
    checkTelegramCommands();
    lastCheckTime = currentTime;
  }

  // Memeriksa waktu dan memicu alarm jika diperlukan
  timeClient.update();

  // Implementasi pemantauan waktu alarm
  // Misalnya, jika alarm diatur pada pukul 07:30 pagi
  if (isAlarmTime()) {
    activateBuzzer();  // Aktifkan buzzer
  } else {
    digitalWrite(buzzerPin, LOW);  // Matikan buzzer
  }

  delay(1000); // Periksa setiap detik
}

void checkTelegramCommands() {
  String chat_id = "YOUR_TELEGRAM_USER_ID";
  if (bot.getNewMessage(chat_id)) {
    String text = bot.getLastMessage();
    if (text.startsWith("/setalarm ")) {
      String alarmTimeStr = text.substring(9);
      if (setAlarm(alarmTimeStr)) {
        bot.sendMessage(chat_id, "Alarm telah diatur: " + alarmTimeStr);
      } else {
        bot.sendMessage(chat_id, "Format waktu tidak valid. Gunakan format HH:MM");
      }
    }
  }
}

bool setAlarm(String alarmTimeStr) {
  // Parsing waktu alarm dari string HH:MM
  int hours = alarmTimeStr.substring(0, 2).toInt();
  int minutes = alarmTimeStr.substring(3).toInt();

  if (hours >= 0 && hours < 24 && minutes >= 0 && minutes < 60) {
    // Menyimpan waktu alarm ke EEPROM
    EEPROM.write(eepromAddress, hours);
    EEPROM.write(eepromAddress + 1, minutes);
    EEPROM.commit();
    return true;
  }
  return false;
}

void loadAlarmFromEEPROM() {
  int hours = EEPROM.read(eepromAddress);
  int minutes = EEPROM.read(eepromAddress + 1);
  // Lakukan validasi waktu yang dibaca dari EEPROM jika diperlukan
  // Misalnya, pastikan nilai hours dan minutes valid
  // ...
}

bool isAlarmTime() {
  int hours = timeClient.getHours();
  int minutes = timeClient.getMinutes();
  int alarmHours, alarmMinutes;
  loadAlarmFromEEPROM();
  return (hours == alarmHours && minutes == alarmMinutes);
}

void activateBuzzer() {
  digitalWrite(buzzerPin, HIGH);
  delay(1000);  // Aktifkan buzzer selama 1 detik (sesuaikan sesuai kebutuhan)
  digitalWrite(buzzerPin, LOW);
}
