#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <EEPROM.h>
#include <Servo.h>
#define addrTandon 0
#define addrMandi 10
#define Pagi 0
#define Sore 1
#define EEPROM_CEK 10
byte ival[10];
byte EEbuff[10];

//=========================================  manual setting =====================
//              p, s             {PAGI,SORE}
byte Jwaktu[2] = { 8, 16 };  // JAM
byte Mwaktu[2] = { 0, 0 };   // MENIT
byte AHari[2] = { 0, 0 };    // {hari Terakhir pengisian , hari mandi terakhir}
byte minAir = 2;             // jarak minimal air habis
byte TangkiAir = 5;          //5 hari jarak antara air akan habis pada toren atau penampungaan
//===============================================================================

Servo myservo;
#define pinServo D4
#define pinPompa D5

#define pinSensor A0
int sensorValue = 0;
float tinggiAir = 0;
float sensorVoltage = 0;
int nilaiMax = 1023;
float panjangSensor = 4.0;


// Wifi network station credentials
#define WIFI_SSID "Lt 4"
#define WIFI_PASSWORD "Mess9876"
// Telegram BOT Token (Get from Botfather)
#define BOT_TOKEN "6668183063:AAGD9zc2Es_w_xxgSdyvrK3FpOhIVzldztE"
const long utcOffsetInSeconds = 25200;
char daysOfTheWeek[7][12] = { "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu" };


WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "id.pool.ntp.org", utcOffsetInSeconds);


const unsigned long BOT_MTBS = 500;  // mean time between scan messages
X509List cert(TELEGRAM_CERTIFICATE_ROOT);
WiFiClientSecure secured_client;
UniversalTelegramBot bot(BOT_TOKEN, secured_client);
unsigned long bot_lasttime;  // last time messages' scan has been done

unsigned int Jam;
unsigned int Menit;
unsigned int Detik;
unsigned int Hari;
String chat_id;
unsigned long NTP_epoc;
String jamNow;
const int ledPin = LED_BUILTIN;
int ledStatus = 0;

int Lmakan = 0;
int Lminum = 0;
int LSair = 0;
int LockNotif = 0;

void handleNewMessages(int numNewMessages) {
  Serial.print("handleNewMessages ");
  Serial.println(numNewMessages);

  for (int i = 0; i < numNewMessages; i++) {
    chat_id = bot.messages[i].chat_id;
    String text = bot.messages[i].text;

    String from_name = bot.messages[i].from_name;
    if (from_name == "")
      from_name = "Guest";

    if (text == "/ledon") {
      digitalWrite(ledPin, LOW);  // turn the LED on (HIGH is the voltage level)
      ledStatus = 1;
      bot.sendMessage(chat_id, "Led is ON", "");
    }

    if (text == "/ledoff") {
      ledStatus = 0;
      digitalWrite(ledPin, HIGH);  // turn the LED off (LOW is the voltage level)
      bot.sendMessage(chat_id, "Led is OFF", "");
    }


    if (text == "/kasihMakan") {
      bot.sendMessage(chat_id, "Memberi makan ", "");
      makan();
    }

    if (text == "/kasihMinum") {
      bot.sendMessage(chat_id, "Memberi minumm ", "");
      minum();
    }


    if (text == "/tidakMakan") {
      bot.sendMessage(chat_id, "Memberi makan ", "");
    }


    if (text == "/tidakMinum") {
      bot.sendMessage(chat_id, "Memberi makan ", "");
    }


    if (text == "/sensorAirON") {
      bot.sendMessage(chat_id, "Notification Sensor air ON ", "");
      LSair = 0;
    }


    if (text == "/sensorAirOFF") {
      bot.sendMessage(chat_id, "Notification Sensor air OFF ", "");
      LSair = 1;
    }
    if (text == "/status") {
      timeClient.update();
      String Msg = "Halo, " + from_name + ".\n";
      Msg += "Waktu : " + String(daysOfTheWeek[timeClient.getDay()]) + ", " + String(timeClient.getHours()) + ":" + String(timeClient.getMinutes()) + " \n";
      
      Msg += " \n ";
      Msg += " \n";
      bot.sendMessage(chat_id, Msg, "Markdown");
    }

    if (text == "/isiTandon") {
       saveToEEPROM(addrTandon, Hari);
       readFromEEPROM(addrTandon);
       bot.sendMessage(chat_id, "tandon air di isi ", "");
      //testing
    }

    if (text == "/start") {
      String welcome = "Selamat datang di Sangkar Burung Telegram, " + from_name + ".\n";
      welcome += "Sialahakan pilih menu yang di inginkan \n\n";
      welcome += "/status : menampilkan keadaan sekarang\n";
      welcome += "/ledon  : menghidupkan LED pada Wemos\n";
      welcome += "/ledoff : mematikan LED pada Wemos\n";
      welcome += "/isiTandon : set hari ini sebagai pengisian tandon ke full untuk 5 hari ke depan \n";
      welcome += "/kasihMakan  : memberi makan \n";
      welcome += "/tidakMakan  : tidak memberi makan \n";
      welcome += "/kasihMinum  : memberi minum \n";
      welcome += "/tidakMinum  : tidak memberi minum\n";
      welcome += "/sensorAirON : menghidupkan sensor air dan pemberitahuan \n";
      welcome += "/sensorAirOFF : mematikan sensor air dan pemberitahuan\n";

      bot.sendMessage(chat_id, welcome, "Markdown");
    }
  }
}

void saveToEEPROM(byte address, byte data) {
  EEPROM.put(address, data);
  if (EEPROM.commit()) {
    Serial.println("EEPROM successfully committed");
    delay(100);
  } else {
    Serial.println("ERROR! EEPROM commit failed");
    delay(20);
  }
}

byte readFromEEPROM(byte address) {
  byte nilaieep;
  nilaieep = EEPROM.read(address);
  Serial.print("EEPROM addr");
  Serial.print(address);
  Serial.print(" : ");
  Serial.print(nilaieep);
  Serial.println("  ");
  return nilaieep;
}

void initEE() {
  EEPROM.begin(100);
  Serial.println("EEPROM init");
  if (readFromEEPROM(0) != EEPROM_CEK) {
    saveToEEPROM(0, EEPROM_CEK);
  }
}

void makan() {
  myservo.write(90);  // Menggerakkan servo ke posisi 90 derajat
  delay(500);         // waitiing
  myservo.write(0);   // Menggerakkan servo ke posisi 0 derajat
  delay(1000);        //
}

void minum() {
  digitalWrite(pinPompa, HIGH);
  delay(2000);
  digitalWrite(pinPompa, LOW);
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  pinMode(ledPin, OUTPUT);  // initialize digital ledPin as an output.
  pinMode(pinSensor, INPUT_PULLUP);
  pinMode(pinServo, OUTPUT);
  pinMode(pinPompa, OUTPUT);
  delay(10);
  myservo.attach(pinServo);
  digitalWrite(ledPin, HIGH);  // initialize pin as off (active LOW)

  // attempt to connect to Wifi network:
  configTime(0, 0, "pool.ntp.org");       // get UTC time via NTP
  secured_client.setTrustAnchors(&cert);  // Add root certificate for api.telegram.org
  Serial.print("Connecting to Wifi SSID ");
  Serial.print(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.print("\nWiFi connected. IP address: ");
  Serial.println(WiFi.localIP());
  timeClient.begin();
  // Check NTP/Time, usually it is instantaneous and you can delete the code below.
  Serial.print("Retrieving time: ");
  time_t now = time(nullptr);
  while (now < 24 * 3600) {
    Serial.print(".");
    delay(100);
    now = time(nullptr);
  }
  Serial.println(now);
  initEE();
  // hariPengisian = readFromEEPROM(addrTandon);
}

float sensorAir() {
  sensorValue = analogRead(pinSensor);
  tinggiAir = sensorValue * panjangSensor / nilaiMax;
  Serial.println(tinggiAir);
  return tinggiAir;
}


void loop() {


  if (millis() - bot_lasttime > BOT_MTBS) {
    Serial.print("wait.....");
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

    while (numNewMessages) {
      Serial.println("got response");
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }
    Serial.println(".....");
    NTP_cek();
    alarm();
    bot_lasttime = millis();
  }
}


void alarm() {
  //alarm pakan burung mau habis
  //Pagi
  if (Jam == 12) {
    LockNotif = 0;
  }
  if (Jam == Jwaktu[0] && Menit == Mwaktu[0]) {
    //alarm pakan burung mau habis
    Serial.println("alarm pakan burung mau habis");
    String Msg;
    Msg += "pakan burung habis\n ";
    if (LockNotif == 0) {
      bot.sendMessage(chat_id, Msg, "Markdown");
      LockNotif = 1;
    }
  }

  //alarm pakan burung mau habis
  //Sore
  if (Jam == Jwaktu[1] && Menit == Mwaktu[1]) {
    Serial.println("alarm pakan burung mau habis");
    //alarm pakan burung mau habis
    String Msg;
    Msg += "pakan burung habis\n ";
    if (LockNotif == 0) {
      bot.sendMessage(chat_id, Msg, "Markdown");
      LockNotif = 1;
    }
  }

  // peringatan wadah minum habis sensor air
  if (LSair == 0 && sensorAir() <= minAir) {
    String Msg;
    Serial.println("peringatan wadah minum habis sensor air");
    Msg += "peringatan wadah minum habis sensor air\n ";
    bot.sendMessage(chat_id, Msg, "Markdown");
    // alarm air minum habis
  }
  //peringatan waktu tangki air habis
  byte Chari = TangkiAir + AHari[0];
  if (Chari > 7) {
    Chari = Chari - 7;
  }
  if (Hari == Chari) {
    String Msg;
    Serial.println("tandon air mau habis");
    Msg += "tandon air mau habis\n ";
    if (LockNotif == 0)
      bot.sendMessage(chat_id, Msg, "Markdown");
  }
}
void NTP_cek() {
  timeClient.update();
  NTP_epoc = (timeClient.getHours() * 60 * 60) + (timeClient.getMinutes() * 60) + timeClient.getSeconds();

  Serial.println("====================================================================================================================");
  Serial.print(daysOfTheWeek[timeClient.getDay()]);
  Serial.print(", ");
  Serial.print(timeClient.getHours());
  Serial.print(":");
  Serial.print(timeClient.getMinutes());
  Serial.print(":");
  Serial.println(timeClient.getSeconds());

  Hari = timeClient.getDay();
  Jam = timeClient.getHours();
  Menit = timeClient.getMinutes();
  Detik = timeClient.getSeconds();

  Serial.println("====================================================================================================================");
  Serial.println("");
}
